### About ###
- Simple demonstration of Spring-boot, JPA, Auth token filter, JSON API via JsonViews and integration tests
- Manages contacts and their phone numbers

###Notes
- Run with `mvn spring-boot:run`
- Using h2 database which will be created in ./mainDb.h2
- No initial data: use `POST /register` endpoint with `{"username":"root","password":"root"}`
- Tokens are stored in memory only.
- Passwords are plain text for simplicity.
- Few integration tests