package eu.udrzal.contact.rest;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import eu.udrzal.contact.service.ContactService;
import eu.udrzal.contact.service.UserService;

@RestController
public class UserRestController {
    @Autowired
    ContactService contactService;

    @Autowired
    UserService userService;

    @RequestMapping(path = "/user", method = RequestMethod.POST)
    public ResponseEntity createToken(@RequestBody Map<String, String> body) {
        String username = body.getOrDefault("username", null);
        String password = body.getOrDefault("password", null);
        String token = userService.createToken(username, password);
        if (token == null) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } else {
            HashMap<Object, Object> map = new HashMap<>();
            map.put("token", token);
            return new ResponseEntity(map, HttpStatus.CREATED);
        }
    }

    @RequestMapping(path = "/register", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.CREATED)
    public void register(@RequestBody Map<String, String> body) {
        String username = body.getOrDefault("username", null);
        String password = body.getOrDefault("password", null);
        userService.createUser(username, password);
    }
}