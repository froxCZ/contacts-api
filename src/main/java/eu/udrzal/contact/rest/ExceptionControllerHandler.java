package eu.udrzal.contact.rest;

import java.util.HashMap;
import java.util.Map;

import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionControllerHandler {
    @ExceptionHandler(value = {AccessDeniedException.class})
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public Map<String, String> accessDenied() {
        Map<String, String> responseBody = new HashMap<>();
        responseBody.put("message", "You don't have access to this resource.");
        return responseBody;
    }

    @ExceptionHandler(value = {Throwable.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Map<String, String> unknownException() {
        Map<String, String> responseBody = new HashMap<>();
        responseBody.put("message", "Internal server error.");
        return responseBody;
    }

    @ExceptionHandler(value = {ConstraintViolationException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Map<String, String> constraintValidationException(ConstraintViolationException exception) {
        Map<String, String> responseBody = new HashMap<>();
        responseBody.put("message", exception.getConstraintViolations().iterator().next().getMessage());
        return responseBody;
    }
}
