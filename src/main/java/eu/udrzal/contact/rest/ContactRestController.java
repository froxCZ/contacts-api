package eu.udrzal.contact.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;
import eu.udrzal.contact.model.Contact;
import eu.udrzal.contact.model.Views;
import eu.udrzal.contact.service.ContactService;

@RestController
public class ContactRestController {
    @Autowired
    ContactService contactService;

    @RequestMapping(path = "/contacts", method = RequestMethod.POST)
    @JsonView(Views.IdOnly.class)
    @ResponseStatus(value = HttpStatus.CREATED)
    public Contact createContact(@JsonView(Views.Overview.class) @RequestBody Contact newContact) {
        return contactService.createContact(newContact);
    }

    @RequestMapping(path = "/contacts/{contactId}", method = RequestMethod.POST)
    @JsonView(Views.IdOnly.class)
    public Contact updateContact(@PathVariable(value = "contactId") Long contactId,
                                 @JsonView(Views.Overview.class) @RequestBody Contact newContactName) {
        return contactService.updateContactName(contactId, newContactName);
    }

    @RequestMapping(path = "/contacts/{contactId}", method = RequestMethod.DELETE)
    public HashMap<Object, Object> deleteContact(@PathVariable(value = "contactId") Long contactId) {
        contactService.deleteContact(contactId);
        return new HashMap<>();
    }

    @RequestMapping(path = "/contacts/{id}/entries", method = RequestMethod.POST)
    @JsonView(Views.IdOnly.class)
    @ResponseStatus(value = HttpStatus.CREATED)
    public Contact addPhoneNumber(@PathVariable(value = "id") Long contactId,@RequestBody Map<String,String> body) {
        String phone = body.getOrDefault("phone",null);
        return contactService.addPhoneNumberToContact(contactId, phone);
    }

    @RequestMapping(path = "/contacts", method = RequestMethod.GET)
    @JsonView(Views.Detail.class)
    public List<Contact> getContacts() {
        return contactService.getContacts();
    }
}