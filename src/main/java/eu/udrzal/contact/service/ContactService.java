package eu.udrzal.contact.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;

import eu.udrzal.contact.model.Contact;
import eu.udrzal.contact.model.PhoneNumber;
import eu.udrzal.contact.model.User;

@Service
public class ContactService {
    @Autowired
    private EntityManager em;

    @Autowired
    UserService userService;

    @Transactional
    public Contact createContact(Contact newContact) {
        newContact.setUser(em.find(User.class, userService.getLoggedInUsername()));
        em.persist(newContact);
        return newContact;
    }

    @Transactional
    public List<Contact> getContacts() {
        Query query = em.createNamedQuery("findUserContacts")
                .setParameter("username", userService.getLoggedInUsername());
        return query.getResultList();

    }

    @Transactional
    public Contact updateContactName(Long contactId, Contact newContactName) {
        Contact contact = getOwnedContactOrThrow(contactId);
        contact.setFirstName(newContactName.getFirstName());
        contact.setLastName(newContactName.getLastName());
        return contact;
    }

    @Transactional
    public Contact addPhoneNumberToContact(Long contactId, String phone) {
        Contact contact = getOwnedContactOrThrow(contactId);
        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setPhoneNumber(phone);
        phoneNumber.setContact(contact);
        em.persist(phoneNumber);
        return contact;
    }

    @Transactional
    public void deleteContact(Long contactId) {
        Contact contact = getOwnedContactOrThrow(contactId);
        em.remove(contact);
    }

    private Contact getOwnedContactOrThrow(Long contactId) {
        Contact contact = em.find(Contact.class, contactId);

        if (contact == null ||
                !StringUtils.equals(userService.getLoggedInUsername(), contact.getUser().getUsername())) {
            throw new AccessDeniedException("Not your contact.");
        }
        return contact;
    }
}
