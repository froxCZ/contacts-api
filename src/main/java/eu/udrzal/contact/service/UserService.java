package eu.udrzal.contact.service;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import eu.udrzal.contact.model.User;

@Service
public class UserService {
    /**
     * In memory token map. For production usage it should be either in database, or better in some distributed cache
     * that would make the tokens expire.
     */
    Map<String, String> tokenMap = new ConcurrentHashMap<>();

    @Autowired
    private EntityManager em;

    /**
     * Returns token or null, if credentials are wrong.
     */
    public String createToken(String username, String password) {
        try {
            User user = (User) em.createQuery("FROM User u WHERE u.username = :username AND u.passwordHash" +
                    " = :password")
                    .setParameter("username", username)
                    .setParameter("password", password)
                    .getSingleResult();

            String token = UUID.randomUUID().toString();
            tokenMap.put(token, user.getUsername());
            return token;
        } catch (NoResultException e) {
            return null;
        }

    }

    public String getLoggedInUsername() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            Object principal = auth.getPrincipal();
            if (principal != null) {
                return principal.toString();
            }
        }
        return null;
    }

    public String getUsernameForToken(String token) {
        return tokenMap.get(token);
    }

    @Transactional
    public void createUser(String username, String password) {
        User user = new User();
        user.setUsername(username);
        user.setPasswordHash(password);
        em.persist(user);
    }


}
