package eu.udrzal.contact.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonValue;
import eu.udrzal.contact.model.validator.InternationalPhoneNumber;

@Entity
public class PhoneNumber {
    @Id
    @GeneratedValue
    Long id;

    @InternationalPhoneNumber
    String phoneNumber;

    @ManyToOne
    @JoinColumn(name = "contactId")
    Contact contact;

    @JsonValue
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }
}
