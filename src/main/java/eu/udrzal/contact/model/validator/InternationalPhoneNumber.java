package eu.udrzal.contact.model.validator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = InternationalPhoneNumberValidator.class)
public @interface InternationalPhoneNumber {
    String message() default "Not a valid international phone number.";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}