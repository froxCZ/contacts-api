package eu.udrzal.contact.model.validator;

import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class InternationalPhoneNumberValidator implements ConstraintValidator<InternationalPhoneNumber, String> {
    /**
     * Regexp to match international numbers. Not perfect.
     */
    private final static String PHONE_NUMBER_REGEXP = "^((\\+{1}[\\d]{1,3})|([0]{2}))([\\s])?" +
            "(([\\(]{1}[\\d]{2,3}[\\)]{1}[\\s]?)|([\\d]{2,3}[\\s]?))?" +
            "(\\d[\\-\\.\\s]?){2,16}$";
    public void initialize(InternationalPhoneNumber constraint) {
    }


    public boolean isValid(String phoneNumber, ConstraintValidatorContext context) {
        return Pattern.compile(PHONE_NUMBER_REGEXP).matcher(phoneNumber).find();
    }
}
