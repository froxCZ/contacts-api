package eu.udrzal.contact.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonView;

@NamedQueries(@NamedQuery(name = "findUserContacts", query = "SELECT DISTINCT c FROM Contact c LEFT JOIN FETCH c.phoneNumbers " +
        "JOIN FETCH c.user u WHERE u.username = :username"))
@Entity
public class Contact {
    @Id
    @GeneratedValue
    @JsonView(Views.IdOnly.class)
    Long id;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    User user;

    @JsonView(Views.Overview.class)
    String firstName;

    @JsonView(Views.Overview.class)
    String lastName;

    @JsonView(Views.Detail.class)
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "contact", cascade = CascadeType.ALL)
    Set<PhoneNumber> phoneNumbers;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Set<PhoneNumber> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(Set<PhoneNumber> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }
}
