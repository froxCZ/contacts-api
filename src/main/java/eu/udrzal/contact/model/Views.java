package eu.udrzal.contact.model;

public class Views {
    public static class IdOnly {
    }

    public static class Overview extends IdOnly {
    }

    public static class Detail extends Overview {
    }
}
