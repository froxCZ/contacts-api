package eu.udrzal.contact.integration;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.databind.JsonNode;
import eu.udrzal.contact.model.UserCredentials;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:test.properties")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Sql("classpath:test_import.sql")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class ContactIntegrationTest extends BaseIntegrationTest {

    @Test
    public void addingContactAndNumberTest() {
        setToken(ROOT_USER_CREDENTIALS);
        ResponseEntity<JsonNode> response;

        response = getContacts();
        JsonNode contacts = response.getBody();
        assertThat(contacts.size(), is(3));

        response = createContact("fName", "lName");
        assertThat(response.getStatusCode(), is(HttpStatus.CREATED));
        int id = response.getBody().get("id").asInt();

        response = addNumber(id, "+420777777777");
        assertThat(response.getStatusCode(), is(HttpStatus.CREATED));

        response = getContacts();
        contacts = response.getBody();
        assertThat(contacts.size(), is(4));

    }

    @Test
    public void addNumberToOtherContact() {
        setToken(ROOT_USER_CREDENTIALS);
        ResponseEntity<JsonNode> response;

        response = addNumber(4, "+420777777777");
        assertThat(response.getStatusCode(), is(HttpStatus.FORBIDDEN));

        response = addNumber(100, "+420777777777");
        assertThat(response.getStatusCode(), is(HttpStatus.FORBIDDEN));

    }

    @Test
    public void multiplePhoneNumbers() {
        setToken(ROOT2_USER_CREDENTIALS);
        ResponseEntity<JsonNode> response;

        response = createContact("multiple", "contacts");
        assertThat(response.getStatusCode(), is(HttpStatus.CREATED));
        int id = response.getBody().get("id").asInt();

        response = addNumber(id, "+420331554879");
        assertThat(response.getStatusCode(), is(HttpStatus.CREATED));

        response = addNumber(id, "+420444888797");
        assertThat(response.getStatusCode(), is(HttpStatus.CREATED));
        JsonNode contacts = getContacts().getBody();

        assertThat(contacts.size(), is(2));

        for (JsonNode contact : contacts) {
            System.out.println(contact);
            if (contact.get("first_name").asText().equals("multiple")) {
                JsonNode phoneNumbers = contact.get("phone_numbers");
                assertThat(phoneNumbers.size(), is(2));
            }
        }
    }

    @Test
    public void invalidPhoneNumber() {
        setToken(ROOT_USER_CREDENTIALS);
        ResponseEntity<JsonNode> response;
        response = addNumber(1, "44464a");
        assertThat(response.getStatusCode(), is(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void deletingContacts() {
        UserCredentials credentials = new UserCredentials();
        credentials.setUsername("userDeleteTest");
        credentials.setPassword("userDeleteTest");
        setToken(credentials);
        ResponseEntity<JsonNode> response;
        assertThat(getContacts().getBody().size(), is(3));
        assertThat(deleteContact(5).getBody().toString(),is("{}"));
        deleteContact(6);
        deleteContact(7);

        assertThat(getContacts().getBody().size(), is(0));


    }

}