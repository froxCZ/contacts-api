package eu.udrzal.contact.integration;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.databind.JsonNode;
import eu.udrzal.contact.model.UserCredentials;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:test.properties")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Sql("classpath:test_import.sql")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class AuthIntegrationTest extends BaseIntegrationTest {


    @Test
    public void registrationTest() {
        token = null;
        ResponseEntity<JsonNode> contactsResponse = getContacts();
        assertThat(contactsResponse.getStatusCode(), is(HttpStatus.FORBIDDEN));
        register(USER3_CREDENTIALS);

        setToken(USER3_CREDENTIALS);

        contactsResponse = getContacts();
        assertThat(contactsResponse.getStatusCode(), is(HttpStatus.OK));
        assertThat(contactsResponse.getBody().size(), is(0));
    }

    @Test
    public void loginFail() {
        ResponseEntity<String> response = this.restTemplate.postForEntity("/user", USER4_CREDENTIALS, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED));
    }

    @Test
    public void registrationFail() {
        UserCredentials credentials = getRandomCredentials();
        ResponseEntity<String> response;
        response = this.restTemplate.postForEntity("/register", credentials, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.CREATED));

        response = this.restTemplate.postForEntity("/register", credentials, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.INTERNAL_SERVER_ERROR));
    }

}