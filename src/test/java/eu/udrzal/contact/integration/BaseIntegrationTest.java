package eu.udrzal.contact.integration;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.udrzal.contact.model.Contact;
import eu.udrzal.contact.model.UserCredentials;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertTrue;

public class BaseIntegrationTest {
    UserCredentials ROOT_USER_CREDENTIALS;
    UserCredentials ROOT2_USER_CREDENTIALS;
    UserCredentials USER3_CREDENTIALS;
    UserCredentials USER4_CREDENTIALS;

    @Autowired
    TestRestTemplate restTemplate;

    String token;

    @Before
    public void before() {
        ROOT_USER_CREDENTIALS = new UserCredentials();
        ROOT_USER_CREDENTIALS.setUsername("root");
        ROOT_USER_CREDENTIALS.setPassword("root");

        ROOT2_USER_CREDENTIALS = new UserCredentials();
        ROOT2_USER_CREDENTIALS.setUsername("root2");
        ROOT2_USER_CREDENTIALS.setPassword("root2");

        USER3_CREDENTIALS = new UserCredentials();
        USER3_CREDENTIALS.setUsername("user3");
        USER3_CREDENTIALS.setPassword("user3");

        USER4_CREDENTIALS = new UserCredentials();
        USER4_CREDENTIALS.setUsername("user4");
        USER4_CREDENTIALS.setPassword("user4");

    }

    static UserCredentials getRandomCredentials() {
        UserCredentials credentials = new UserCredentials();
        credentials.setUsername(UUID.randomUUID().toString());
        credentials.setPassword("password");
        return credentials;
    }

    void register(UserCredentials userCredentials) {
        ResponseEntity<String> response = this.restTemplate.postForEntity("/register", userCredentials, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.CREATED));
    }

    void setToken(UserCredentials userCredentials) {
        ResponseEntity<String> response = this.restTemplate.postForEntity("/user", userCredentials, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.CREATED));
        String token = toJson(response).get("token").asText();
        assertTrue(StringUtils.isNotBlank(token));
        this.token = token;
    }

    ResponseEntity<JsonNode> createContact(String firstname, String lastname) {
        Contact contact = new Contact();
        contact.setFirstName(firstname);
        contact.setLastName(lastname);
        HttpEntity<Contact> entity = new HttpEntity<Contact>(contact, getTokenHeader());
        ResponseEntity<JsonNode> response = restTemplate.exchange("/contacts", HttpMethod.POST, entity, JsonNode.class);
        return response;
    }

    ResponseEntity<JsonNode> addNumber(int contactId, String number) {
        Map<String, String> phoneMap = new HashMap<>();
        phoneMap.put("phone", number);
        HttpEntity<Map> entity = new HttpEntity<Map>(phoneMap, getTokenHeader());
        ResponseEntity<JsonNode> response = restTemplate.exchange(
                "/contacts/" + contactId + "/entries",
                HttpMethod.POST,
                entity,
                JsonNode.class);
        return response;
    }

    ResponseEntity<JsonNode> getContacts() {
        HttpEntity<String> entity = new HttpEntity<String>("parameters", getTokenHeader());
        ResponseEntity<JsonNode> response = restTemplate.exchange("/contacts", HttpMethod.GET, entity, JsonNode.class);
        return response;
    }

    ResponseEntity<JsonNode> deleteContact(int id) {
        HttpEntity<String> entity = new HttpEntity<String>("parameters", getTokenHeader());
        ResponseEntity<JsonNode> response = restTemplate.exchange("/contacts/"+id, HttpMethod.DELETE, entity, JsonNode.class);
        return response;
    }

    JsonNode toJson(ResponseEntity<String> responseEntity) {
        return toJson(responseEntity.getBody());
    }

    JsonNode toJson(String s) {
        try {
            return new ObjectMapper().readTree(s);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    HttpHeaders getTokenHeader() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Token", token);
        return headers;
    }
}
