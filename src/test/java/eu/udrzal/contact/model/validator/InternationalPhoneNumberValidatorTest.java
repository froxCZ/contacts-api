package eu.udrzal.contact.model.validator;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class InternationalPhoneNumberValidatorTest {
    private final String input;
    private final boolean expected;

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {"464", false},
                {"44464", false},
                {"+1 (555) 123-456", true},
                {"+49 1245 154875", true},
                {"0065-9022-4578", true},
                {"+39 0678345057", true},
                {"+420 777 313 284", true},
                {"+420 777a313 284", false},
        });
    }

    public InternationalPhoneNumberValidatorTest(String input, boolean expected) {
        this.input = input;
        this.expected = expected;
    }

    @Test
    public void test() {
        InternationalPhoneNumberValidator validator = new InternationalPhoneNumberValidator();
        Assert.assertEquals(expected, validator.isValid(input, null));
    }

}